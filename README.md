# RHEL packaging CI components

This project provides CI/CD components to simplify building and testing
RHEL packaging projects that provide a `.spec` file for an upstream project.

## Usage

```yaml
include:
  - component: git.ligo.org/computing/software/rhel/ci/pipeline@main
    inputs:
      redhat_versions:
        - 8
        - 9
      test_install: python3 python3-pytest
      test_script:
        - python3 -m pytest -ra --pyargs myproject
        - myinterface --help
```

## Contributing

Please read about CI/CD components and best practices at: <https://git.ligo.org/help/ci/components/index.html>.

All interactions related to this project should follow the
[LIGO-Virgo-KAGRA Code of Conduct](https://dcc.ligo.org/LIGO-M1900037).

For more details on contributing to this project, see `CONTRIBUTING.md`.
